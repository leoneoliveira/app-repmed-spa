import EventBus from '@/EventBus/event-bus';
import { Bar } from 'vue-chartjs'
var currencyFormatter = require('currency-formatter');

export default {
  ind: 0,
  extends: Bar,
  props: ['labelss', 'dataa', 'ano'],
  mounted () {
    this.renderChart({
      labels: this.labelss,
      datasets: [
        {
          label: 'Repassado',
          backgroundColor: '#4D7498',
          data: this.dataa,
          borderWidth: 2
        }
      ]
    },
    {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }],
        xAxes: [{
          stacked: true
        }]
      },
      title: {
        display: true,
        text: 'Histórico produção em ' + this.ano,
        fontSize: 18,
        fontFamily: 'Arial'
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            this.ind = tooltipItem.index;
            var label = 'Repassado: ' + currencyFormatter.format(tooltipItem.yLabel, { code: 'BRL' });
            return label;
          }
        }
      },
      onClick: function (evt) {
        if (this.getElementAtEvent(evt)[0] !== undefined) {
          EventBus.$emit('MES_HISTORICO_SELECIONADO', this.getElementAtEvent(evt)[0]._index);
        }
      }
    })
  }
}
