export default {

  'LOAD_USER' (state, payload) {
    state.user = payload;
  },
  'UPDATE_USER' (state, payload) {
    state.user.nomeusuario = payload.nomeusuario;
    state.user.fotousuario = payload.fotousuario;
    state.user.descCbousuario = payload.descCbousuario;
  },
  'UPDATE_CALENDARIO' (state, data) {
    state.dataCalendario = data;
  },
  'LOGIN' (state, token) {
    state.isLoggedIn = true;
    state.tokenvue = token;
  },
  'LOGOUT' (state) {
    state.isLoggedIn = false;
    state.tokenvue = [];
    state.user = [];
    state.hospital = [];
    state.dataCalendario = null;
    state.cliente = [];
  },
  'UPDATE_CONVENIO' (state, payload) {
    state.hospital.ds_multi_empresa = payload.ds_multi_empresa;
    state.hospital.cd_multi_empresa = payload.cd_multi_empresa;
    state.hospital.cd_convenio = payload.cd_convenio;
    state.hospital.nm_convenio = payload.nm_convenio;
  },
  'UPDATE_REPASSE' (state, payload) {
    state.hospital.ds_cliente = payload.ds_cliente;
    state.hospital.cd_convenio = payload.cd_convenio;
    state.hospital.nm_convenio = payload.nm_convenio;
    state.hospital.cd_repasse = payload.cd_repasse;
    state.hospital.ds_repasse = payload.ds_repasse;
    state.hospital.cd_pro_fat = payload.cd_pro_fat;
    state.hospital.ds_pro_fat = payload.ds_pro_fat;
  },
  'UPDATE_PROCEDIMENTO' (state, payload) {
    state.hospital.cd_pro_fat = payload.cd_pro_fat;
    state.hospital.ds_pro_fat = payload.ds_pro_fat;
  },
  'LOAD_CLIENTE' (state, payload) {
    state.cliente.cd_cliente = payload.cd_cliente;
    state.cliente.ds_cliente = payload.ds_cliente;
    state.cliente.cd_multi_empresa = payload.cd_multi_empresa;
    state.cliente.nm_multi_empresa = payload.nm_multi_empresa;
    state.cliente.cd_prestador = payload.cd_prestador;
    state.cliente.cd_prestador_chefe = payload.cd_prestador_chefe;
  },
  'ATUALIZA_CLIENTE' (state, payload) {
    state.cliente.cd_cliente = payload.cd_cliente;
    state.cliente.ds_cliente = payload.ds_cliente;
    state.cliente.cd_multi_empresa = payload.cd_multi_empresa;
    state.cliente.nm_multi_empresa = payload.nm_multi_empresa;
  },
  'UPDATE_PRESTADOR_EQUIPE' (state, payload) {
    state.cliente.cd_prestador_equipe_escolhido = payload.cd_prestador_produziu;
    state.cliente.cd_multi_empresa_equipe_escolhido = payload.cd_multi_empresa;
    state.cliente.cd_equipe_escolhido = payload.cd_equipe;
  },
  'UPDATE_PRESTADOR_FLOW' (state, payload) {
    state.cliente.cd_prestador_flow = payload;
    state.cliente.cd_prestador_equipe_escolhido = null;
    state.cliente.cd_multi_empresa_equipe_escolhido = null;
    state.cliente.cd_equipe_escolhido = null;
  },
  'UPDATE_CONVENIO_REPASSE' (state, payload) {
    state.hospital.cd_convenio_repasse = payload.cd_convenio;
    state.hospital.cd_multi_empresa = payload.cd_multi_empresa
  },
  'UPDATE_ATENDIMENTO' (state, payload) {
    state.atendimento = payload;
  }
}
