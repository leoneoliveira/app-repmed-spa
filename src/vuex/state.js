export default {
  user: {
    idusuario: null,
    nomeusuario: null,
    fotousuario: null,
    cbousuario: null,
    descCbousuario: null,
    email: null,
    cpf: null
  },
  dataCalendario: null,
  isLoggedIn: false,
  tokenvue: [],
  hospital: {
    cd_multi_empresa: null,
    ds_multi_empresa: null,
    nm_convenio: null,
    cd_pro_fat: null,
    ds_pro_fat: null,
    cd_repasse: null,
    ds_repasse: null,
    cd_convenio_repasse: null
  },
  cliente: {
    cd_cliente: null,
    ds_cliente: null,
    cd_multi_empresa: null,
    cd_prestador: null,
    cd_prestador_chefe: null,
    nm_multi_empresa: null,
    cd_prestador_equipe_escolhido: null,
    cd_multi_empresa_equipe_escolhido: null,
    cd_equipe_escolhido: null,
    cd_prestador_flow: null
  },
  atendimento: {
    dt_lancamento: null,
    cd_multi_empresa: null,
    ds_multi_empresa: null,
    cd_atendimento: null,
    nm_paciente: null,
    cd_pro_fat: null,
    ds_pro_fat: null,
    cd_conta: null,
    cd_lanca: null
  }
}
