// CommitChart.js
import { Line } from 'vue-chartjs'
var currencyFormatter = require('currency-formatter');

export default {
  extends: Line,
  props: ['labels', 'dataRecebido', 'dataPendente'],
  data () {
    return {
      gradient: null,
      gradient2: null
    }
  },
  mounted () {
    this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450);
    this.gradient2 = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450);
    this.gradient.addColorStop(0, 'rgba(255, 0,0, 0.7)');
    this.gradient.addColorStop(0.5, 'rgba(255, 0, 0, 0.25)');
    this.gradient.addColorStop(1, 'rgba(255, 0, 0, 0)');
    this.gradient2.addColorStop(0, 'rgba(60, 141, 188, 0.9)');
    this.gradient2.addColorStop(0.5, 'rgba(60, 141, 188, 0.25)');
    this.gradient2.addColorStop(1, 'rgba(60, 141, 188, 0)');
    // Overwriting base render method with actual data.
    this.renderChart({
      labels: this.labels,
      datasets: [
        {
          label: 'Repassado',
          borderColor: '#4d7498',
          pointBackgroundColor: '#4d7498',
          pointBorderColor: '#4d7498',
          borderWidth: 1,
          backgroundColor: this.gradient2,
          data: this.dataRecebido
        },
        {
          label: 'Pendente',
          borderColor: '#FC2525',
          pointBackgroundColor: '#FC2525',
          pointBorderColor: '#FC2525',
          borderWidth: 1,
          backgroundColor: this.gradient,
          data: this.dataPendente
        }
      ]
    },
    {
      responsive: true,
      maintainAspectRatio: false,
      elements: {
        line: {
          tension: 0
        }
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            this.ind = tooltipItem.index;
            var label = 'Valor: ' + currencyFormatter.format(tooltipItem.yLabel, { code: 'BRL' });
            return label;
          }
        }
      }
    })
  }
}