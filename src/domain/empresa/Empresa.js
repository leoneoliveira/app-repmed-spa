/* eslint-disable */
export default class Empresa {

  constructor(
    id = '',
    cd_multi_empresa = '',
    nm_multi_empresa = '',
    cd_cliente = '',
    sn_habilitada = '',
    sn_chamados = '',
    sn_sla = '',
    sn_historico = '',
    sn_chat = '',
    tp_chat = '') {

    this.id = id;
    this.cd_multi_empresa = cd_multi_empresa;
    this.nm_multi_empresa = nm_multi_empresa;
    this.cd_cliente = cd_cliente;
    this.sn_habilitada = sn_habilitada;
    this.sn_chamados = sn_chamados;
    this.sn_sla = sn_sla;
    this.sn_historico = sn_historico;
    this.sn_chat = sn_chat;
    this.tp_chat = tp_chat;

  }

}
