/* eslint-disable */
import { HTTP } from '@/common.js';

export default class EmpresaService {

  findAllEmpresa () {
    return HTTP.get('/empresa');
  }

  findByIdCliente (cdCliente) {
    return HTTP.get('/empresa/cliente', {
      params: {
        cd_cliente: cdCliente
      }
    });
  }
}
