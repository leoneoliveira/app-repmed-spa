/* eslint-disable */
import { HTTP } from '@/common.js';

export default class EquipeMedicaService {

  findAllEquipeMedicaPorValor (cd_prestador, data) {
    return HTTP.get('/equipemedica/equipeporvalor', {
      params: {
        cd_prestador: cd_prestador,
        data: data
      }
    });
  }

  findEquipeMedicaValor (cd_prestador, data, cd_cliente) {
    return HTTP.get('/equipemedica/equipevalor', {
      params: {
        cd_prestador: cd_prestador,
        data: data,
        cd_cliente: cd_cliente
      }
    });
  }

  findAllEquipeMedicaPorPrestadoValor (cd_prestador, data, cd_equipe, cd_cliente, cd_multi_empresa) {
    return HTTP.get('/equipemedica/equipeprestadorporvalor', {
      params: {
        cd_prestador: cd_prestador,
        data: data,
        cd_equipe: cd_equipe,
        cd_cliente: cd_cliente,
        cd_multi_empresa: cd_multi_empresa
      }
    });
  }
}
