import {HTTP} from '@/common.js';

export default class RepasseService {
  busca (data, cdcliente, cdprestador) {
    return HTTP.get('/repasse', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    })
  }
  buscaEquipePrestador (data, cdcliente, cdmultiempresa, cdequipe, cdprestador) {
    return HTTP.get('/repasse/equipe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_equipe: cdequipe,
        cd_prestador: cdprestador
      }
    })
  }
  buscaConvenio (data, cdconvenio, cdcliente, cdmultiempresa, cdprestador) {
    return HTTP.get('/repasse/convenio', {
      params: {
        data: data,
        cd_convenio: cdconvenio,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador
      }
    })
  }
  buscaConvenioEquipe (data, cdconvenio, cdcliente, cdmultiempresa, cdprestador, cdequipe) {
    return HTTP.get('/repasse/convenioEquipe', {
      params: {
        data: data,
        cd_convenio: cdconvenio,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador,
        cd_equipe: cdequipe
      }
    })
  }
  buscaConvenioProcedimento (data, cdconvenio, cdprofat, cdcliente, cdmultiempresa, cdprestador) {
    return HTTP.get('/repasse/convenio/procedimento', {
      params: {
        data: data,
        cd_convenio: cdconvenio,
        cd_pro_fat: cdprofat,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador
      }
    })
  }
  buscaConvenioProcedimentoEquipe (data, cdconvenio, cdprofat, cdcliente, cdmultiempresa, cdprestador, cdequipe) {
    return HTTP.get('/repasse/convenio/procedimentoEquipe', {
      params: {
        data: data,
        cd_convenio: cdconvenio,
        cd_pro_fat: cdprofat,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador,
        cd_equipe: cdequipe
      }
    })
  }
  buscaHome (data, cdcliente, cdmultiempresa, cdprestador) {
    return HTTP.get('/repasseHome', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador
      }
    })
  }
  buscaHomeProducao (data, cdcliente, cdprestador) {
    return HTTP.get('/repasseHomeProducao', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    })
  }
  buscaHomeFaturado (data, cdcliente, cdprestador) {
    return HTTP.get('/repasseHomeFaturado', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    })
  }
  buscaHomeGlosaLiberado (data, cdcliente, cdprestador) {
    return HTTP.get('/repasseHomeDetalhe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    })
  }
  buscaHomeHistorico (data, cdcliente, cdmultiempresa, cdprestador) {
    return HTTP.get('/repasseHistorico', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador
      }
    })
  }
  buscaHospitalRepasse (data, cdconvenio, cdprofat, cdcliente, cdmultiempresa, cdprestador) {
    return HTTP.get('/repasse/hospital', {
      params: {
        data: data,
        cd_convenio: cdconvenio,
        cd_pro_fat: cdprofat,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador
      }
    })
  }
  buscaHospitalRepasseEquipe (data, cdconvenio, cdprofat, cdcliente, cdmultiempresa, cdprestador, cdequipeescolhido) {
    return HTTP.get('/repasse/hospitalEquipe', {
      params: {
        data: data,
        cd_convenio: cdconvenio,
        cd_pro_fat: cdprofat,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador,
        cd_equipe: cdequipeescolhido
      }
    })
  }
  buscaRepasseConveio (data, cdconvenio, cdrepasse, cdcliente, cdmultiempresa, cdprestador) {
    return HTTP.get('/repasse/hospital/convenio', {
      params: {
        data: data,
        cd_convenio: cdconvenio,
        cd_repasse: cdrepasse,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador
      }
    })
  }
  buscaRepasseConveioEquipe (data, cdconvenio, cdrepasse, cdcliente, cdmultiempresa, cdprestador, cdequipe) {
    return HTTP.get('/repasse/hospital/convenioEquipe', {
      params: {
        data: data,
        cd_convenio: cdconvenio,
        cd_repasse: cdrepasse,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador,
        cd_equipe: cdequipe
      }
    })
  }
  buscaRepasseManual (data, cdcliente, cdmultiempresa, cdprestador) {
    return HTTP.get('/repasse/hospital/manual', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador
      }
    })
  }
  buscaRepasseManualEquipe (data, cdcliente, cdmultiempresa, cdprestador, cdequipeescolhido) {
    return HTTP.get('/repasse/hospital/manualEquipe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_multi_empresa: cdmultiempresa,
        cd_prestador: cdprestador,
        cd_equipe: cdequipeescolhido
      }
    })
  }
  buscaAtendimento (data, cdcliente, cdprestador) {
    return HTTP.get('/atendimento', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }
  buscaProducao (data, cdcliente, cdprestador) {
    return HTTP.get('/producao/detalhe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaFaturado (data, cdcliente, cdprestador) {
    return HTTP.get('/faturado/detalhe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaLiberado (data, cdcliente, cdprestador) {
    return HTTP.get('/liberado/detalhe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoprod (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaoprod', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoprodChefe (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaoprod/chefe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoprodRecebido (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaoprodrecebido', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoprodRecebidoChefe (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaoprodrecebido/chefe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoprodPendente (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaoprodpendente', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoprodPendenteChefe (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaoprodpendente/chefe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoproduzido (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaoproduzido', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoproduzidoChefe (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaoproduzido/chefe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoRecebidoTabela (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaorecebido', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoRecebidoTabelaChefe (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaorecebido/chefe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoManualTabela (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaomanual', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaVisaoManualTabelaChefe (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaomanual/chefe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscapendente (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaopendente', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscapendenteChefe (data, cdcliente, cdprestador) {
    return HTTP.get('repasse/visaopendente/chefe', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaLiberadoCompetencia (data, cdcliente, cdprestador) {
    return HTTP.get('/liberado/detalhe/competencia', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }

  buscaLiberadoCompetenciaManual (data, cdcliente, cdprestador) {
    return HTTP.get('/liberado/detalhe/competencia/manual', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    });
  }
}