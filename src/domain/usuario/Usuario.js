/* eslint-disable */
export default class Usuario {

  constructor(nome = '', email = '', cpf = '', password = '', codigo = '', conselho = '',
    uf_conselho = '', endereco = '', numero = '', cbos = '', bairro = '', cidade = '',
    estado = '', foto = '') {

    this.nome = nome;
    this.email = email;
    this.cpf = cpf;
    this.password = password;
    this.codigo = codigo;
    this.conselho = conselho;
    this.uf_conselho = uf_conselho;
    this.endereco = endereco;
    this.numero = numero;
    this.cbos = cbos;
    this.bairro = bairro;
    this.cidade = cidade;
    this.estado = estado;
    this.foto = foto;

  }

}
