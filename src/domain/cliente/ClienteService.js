/* eslint-disable */
import { HTTP } from '@/common.js';

export default class ClienteService {

  findAllCliente () {
    return HTTP.get('/cliente');
  }

  pegaDadosCliente (cdhospital, cdcliente){
    return HTTP.get('/cliente/dadosgerais', {
      params: {
        cd_cliente: cdhospital,
        cd_prestador: cdcliente,
      }
    })
  }
}
