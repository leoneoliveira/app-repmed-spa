/* eslint-disable */
export default class Hospital {

  constructor(
    id = '',
    cd_hospital = '',
    ds_cliente = '',
    cgc = '',
    email = '',
    telefone = '',
    cep = '',
    endereco = '',
    cidade = '',
    bairro = '',
    uf = '') {

    this.id = id;
    this.cd_hospital = cd_hospital;
    this.ds_cliente = ds_cliente;
    this.cgc = cgc;
    this.email = email;
    this.telefone = telefone;
    this.cep = cep;
    this.endereco = endereco;
    this.cidade = cidade;
    this.bairro = bairro;
    this.uf = uf;
  }

}
