import { HTTP } from '@/common.js';

export default class EmailService {
  enviarEmail (email) {
    return HTTP.post('/esqueceusenha', email)
  }
}
