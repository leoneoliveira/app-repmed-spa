/* eslint-disable */
export default class UsuarioClienteEmpresa {

  constructor(id_hospital = '', ds_nome_hospital = '', id_empresa = '', ds_nome_empresa = '') {

    this.id_hospital = id_hospital;
    this.ds_nome_hospital = ds_nome_hospital;
    this.id_empresa = id_empresa;
    this.ds_nome_empresa = ds_nome_empresa;
  }

}
