/* eslint-disable */
import { HTTP } from '@/common.js';

export default class UsuarioClienteEmpresaService {

  buscaClienteUsuario (cpf) {
    return HTTP.get('/usuarioempresas/usuariocliente/', {
      params: {
        cpf: cpf
      }
    });
  }

  buscaCliente (cpf) {
    return HTTP.get('/usuarioempresas/cliente/', {
      params: {
        cpf: cpf
      }
    });
  }

  buscaUsuarioEmpresa (cpf, cdcliente) {
    return HTTP.get('/usuarioempresas/usuarioempresa/', {
      params: {
        cpf: cpf,
        cd_cliente: cdcliente
      }
    });
  }

}
