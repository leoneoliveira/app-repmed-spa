import {HTTP} from '@/common.js';

export default class HistoricoService {
  busca (data, cdcliente, cdprestador) {
    return HTTP.get('/historico', {
      params: {
        data: data,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    })
  }
  buscaHistorico (data, mes, cdcliente, cdprestador) {
    return HTTP.get('/historico/mes', {
      params: {
        data: data,
        mes_historico: mes,
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    })
  }
}