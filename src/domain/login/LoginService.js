import { HTTP } from '@/common.js';

export default class LoginService {
  logar (usuario) {
    return HTTP.post('/login', usuario)
  }
  carregarLogin (email) {
    return HTTP.get('/carregaLogin', {
      params: {
        email: email
      }
    })
  }
}
