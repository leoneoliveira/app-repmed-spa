import {HTTP} from '@/common.js';

export default class HospitalService {
  hospitalMultiEmpresa (cdcliente, cdprestador) {
    return HTTP.get('/multi-empresa', {
      params: {
        cd_cliente: cdcliente,
        cd_prestador: cdprestador
      }
    })
  }
}
