import { HTTP } from '@/common.js';

export default class ChamadosService {
  lista () {
    return HTTP.get('/chamados')
  }

  listaByUsuario (id, cliente) {
    return HTTP.get('/chamados/usuario', {
      params: {
        id: id,
        cliente: cliente
      }
    })
  }

  notificacao (id, cliente) {
    return HTTP.get('/chamados/usuario/notificacao', {
      params: {
        id: id,
        cliente: cliente
      }
    })
  }

  limpaNotificacao (id, cliente) {
    return HTTP.get('/chamados/usuario/notificacao/limpar', {
      params: {
        id: id,
        cliente: cliente
      }
    })
  }

  cadastra (chamado) {
    return HTTP.post('/chamados', chamado)
  }

  cadastraResposta (chamado) {
    return HTTP.post('/chamados/resposta/novo', chamado);
  }
}
