import axios from 'axios'
// import store from '@/vuex/store'

export const HTTP = axios.create({
  baseURL: 'http://localhost:8000',
  headers: {
    Authorization: localStorage.getItem('token'),
    'Content-Type': 'application/json',
    'Charset': 'utf-8'
  }
});
/*
export const HTTP = axios.create({
  baseURL: 'http://18.228.14.140:8000/RepMed',
  headers: {
    Authorization: localStorage.getItem('token'),
    'Content-Type': 'application/json',
    'Charset': 'utf-8'
  }
});
/*
export const HTTP = axios.create({
  baseURL: 'http://18.228.14.140:7000/RepMed',
  headers: {
    Authorization: localStorage.getItem('token'),
    'Content-Type': 'application/json',
    'Charset': 'utf-8'
  }
});
*/
