// User
import User from '@/pages/users/User'
import AlterarSenha from '@/pages/users/AlterarSenha'
// Login
import Login from '@/pages/login/Login'
import EsqueceuSenha from '@/pages/login/EsqueceuSenha'
import Registrar from '@/pages/login/Registrar'

// Chamados
import Chamados from '@/pages/chamados/Chamados'

// Notificação
import Configuracoes from '@/pages/configuracoes/Configuracoes'

// telas workflow
// import Workflow from '@/pages/Workflow/Workflow'
import Historico from '@/pages/Workflow/Historico'
import Producao from '@/pages/Workflow/Producao'
import Procedimento from '@/pages/Workflow/Procedimento'
import Procedimentodata from '@/pages/Workflow/Procedimento-data'
import Repasse from '@/pages/Workflow/Repasse'
import ProducaoEquipe from '@/pages/Workflow/ProducaoEquipe'

// Equipe medica
// import ChefeEquipeMedica from '@/pages/equipeMedica/ChefeEquipeMedica'

// Usuario sem hospital e empresa associado
import ClienteEmpresaSolic from '@/pages/clienteEmpresa/clienteEmpresaSolic'

// Atendimento
import Atendimento from '@/pages/atendimentos/Atendimento'

// Detalhamento Home
import ProducaoDetalhe from '@/pages/Workflow/ProducaoDetalhe'
import FaturadoDetalhe from '@/pages/Workflow/FaturadoDetalhe'
import LiberadoDetalhe from '@/pages/Workflow/liberadoDetalhe'

// Ajuda
/*
import Suporte from '@/pages/ajuda/suporte'
import Sobre from '@/pages/ajuda/sobre'
import FAQ from '@/pages/ajuda/Faq'
import Guias from '@/pages/ajuda/Guias'
import Informacoes from '@/pages/ajuda/Informacoes'
import Manuais from '@/pages/ajuda/Manuais'
import Release from '@/pages/ajuda/Release'
*/

// Fluxo
import FluxoTotal from '@/pages/fluxo/FluxoTotal'
import FluxoRepassado from '@/pages/fluxo/FluxoRepassado'
import FluxoTotalChefe from '@/pages/fluxo/FluxoTotalChefe'

import store from '@/vuex/store'

function requireAuth (to, from, next) {
  window.onbeforeunload = function () {
    return 'Você será desconectado do sistema, tem certeza que deseja continuar?';
  }
  if (!store.state.isLoggedIn) {
    window.location = '/login'
  } else {
    next()
  }
}
export const routes = [
  {
    path: '*',
    menu: false,
    redirect: 'login'
  },
  {
    path: '/home',
    name: 'Home',
    component: FluxoTotal,
    titulo: 'Home',
    icon: 'mv-basico-casa',
    menu: true,
    beforeEnter: requireAuth
  },
  {
    path: '/homeChefe',
    name: '',
    component: FluxoTotalChefe,
    titulo: 'Home',
    icon: 'mv-basico-casa',
    menu: true,
    beforeEnter: requireAuth
  },
  /*
  {
    path: '/Workflow',
    titulo: 'Workflow',
    menu: false,
    submenu: true,
    beforeEnter: requireAuth,
    icon: 'mv-basico-mapa',
    subRoutes: [
      {
        path: '/workflow/producao',
        component: Producao,
        titulo: 'Produção',
        menu: true,
        name: 'Produção',
        beforeEnter: requireAuth
      },
      {
        path: '/workflow/historico',
        component: Historico,
        titulo: 'Histórico',
        menu: true,
        name: 'historico',
        beforeEnter: requireAuth
      },
      {
        path: '/fluxo/repassado',
        component: FluxoRepassado,
        titulo: 'Repassado',
        menu: true,
        name: 'Repassado',
        beforeEnter: requireAuth
      }
    ]
  },
  */
  {
    path: '/workflow/producao',
    component: Producao,
    titulo: 'Produção',
    menu: true,
    icon: 'mv-basico-mapa',
    name: 'Produção',
    beforeEnter: requireAuth
  },
  {
    path: '/workflow/historico',
    component: Historico,
    titulo: 'Histórico',
    menu: true,
    icon: 'mv-basico-historico',
    name: 'Histórico',
    beforeEnter: requireAuth
  },
  {
    path: '/fluxo/repassado',
    component: FluxoRepassado,
    titulo: 'Repassado',
    menu: true,
    icon: 'mv-basico-faturamento',
    name: 'Repassado',
    beforeEnter: requireAuth
  },
  /*
  {
    path: '/workflow/historico',
    name: 'Histórico',
    component: Historico,
    titulo: 'Histórico',
    beforeEnter: requireAuth
  },
  */
  {
    path: '/workflow/producao/procedimento',
    name: 'Procedimento',
    component: Procedimento,
    titulo: 'Procedimento',
    beforeEnter: requireAuth
  },
  {
    path: '/workflow/producao/procedimento/data',
    name: 'Procedimento por data',
    component: Procedimentodata,
    titulo: 'procedimento',
    beforeEnter: requireAuth
  },
  {
    path: '/workflow/producao/repasse',
    name: 'Repasse',
    component: Repasse,
    titulo: 'Repasse',
    beforeEnter: requireAuth
  },
  {
    path: '/workflow/producaoEquipe',
    name: 'Produção Usuário',
    component: ProducaoEquipe,
    titulo: 'ProducaoEquipe',
    beforeEnter: requireAuth
  },
  {
    path: '/atendimento',
    name: 'Atendimentos',
    component: Atendimento,
    titulo: 'Atendimentos',
    icon: 'mv-basico-buscar_cadastro_geral',
    menu: true,
    beforeEnter: requireAuth
  },
  {
    path: '/producao/detalhe',
    name: 'Produção Detalhe',
    component: ProducaoDetalhe,
    titulo: 'Produção Detalhe',
    beforeEnter: requireAuth
  },
  {
    path: '/faturado/detalhe',
    name: 'Faturado Detalhe',
    component: FaturadoDetalhe,
    titulo: 'Faturado Detalhe',
    beforeEnter: requireAuth
  },
  {
    path: '/liberado/detalhe',
    name: 'Liberado Detalhe',
    component: LiberadoDetalhe,
    titulo: 'Liberado Detalhe',
    beforeEnter: requireAuth
  },
  /*
  {
    path: '/chefeEquipeMedica',
    name: 'Equipe Médica',
    component: ChefeEquipeMedica,
    titulo: 'Equipe Médica',
    icon: 'mv-basico-equipe',
    menu: true,
    beforeEnter: requireAuth
  },
  */
  {
    path: '/login',
    name: 'login',
    component: Login,
    titulo: 'Login',
    menu: false
  },
  {
    path: '/esqueceusenha',
    name: 'EsqueceuSenha',
    component: EsqueceuSenha,
    titulo: 'EsqueceuSenha',
    menu: false
  },
  {
    path: '/registrar',
    name: 'Registrar',
    component: Registrar,
    titulo: 'Registrar',
    menu: false
  },
  {
    path: '/usuario',
    name: 'Editar Usuário',
    component: User,
    titulo: 'Usuário',
    icon: 'mv-basico-usuario',
    menu: true,
    beforeEnter: requireAuth
  },
  {
    path: '/alterarsenha',
    name: 'Alterar Senha',
    component: AlterarSenha,
    titulo: 'Alterar Senha',
    beforeEnter: requireAuth
  },
  {
    path: '/chamados',
    name: 'Chamados',
    component: Chamados,
    titulo: 'Chamados',
    icon: 'mv-basico-mensagem',
    menu: true,
    beforeEnter: requireAuth
  },
  {
    path: '/configuracoes',
    name: 'Configurações',
    component: Configuracoes,
    titulo: 'Configurações',
    icon: 'mv-basico-configuracoes',
    menu: true,
    beforeEnter: requireAuth
  },
  /*
  {
    path: '/ajuda/suporte',
    component: Suporte,
    titulo: 'Suporte',
    name: 'Suporte'
  },
  {
    path: '/ajuda/sobre',
    component: Sobre,
    titulo: 'Sobre',
    name: 'Sobre'
  },
  {
    path: '/ajuda/faq',
    component: FAQ,
    titulo: 'FAQ',
    name: 'FAQ'
  },
  {
    path: '/ajuda/guias',
    component: Guias,
    titulo: 'Guias',
    name: 'Guias'
  },
  {
    path: '/ajuda/manuais',
    component: Manuais,
    titulo: 'Manuais',
    name: 'Manuais'
  },
  {
    path: '/ajuda/release',
    component: Release,
    titulo: 'Release',
    name: 'Release'
  },
  {
    path: '/ajuda/informacoes',
    component: Informacoes,
    titulo: 'Informacoes',
    name: 'Informacoes'
  },
  */
  {
    path: '/solicitarAcesso',
    name: 'Solicitar Acesso',
    component: ClienteEmpresaSolic,
    titulo: 'solicitarAcesso',
    menu: false,
    beforeEnter: requireAuth
  },
  {
    path: '/fluxo/producao',
    component: FluxoTotal,
    titulo: 'FluxoTotal',
    name: 'FluxoTotal',
    beforeEnter: requireAuth
  },
  {
    path: '/fluxo/repassado',
    component: FluxoRepassado,
    titulo: 'FluxoRepassado',
    name: 'Total Repassado',
    beforeEnter: requireAuth
  }
]
