// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import VueRouter from 'vue-router'
import { routes } from '@/router/router'
import VeeValidate from 'vee-validate'
import msg from './pt_BR'
import vSelect from 'vue-select'
import Vuex from 'vuex'
import store from './vuex/store'
import { sync } from 'vuex-router-sync'
import VueCharts from 'vue-chartjs'
import Loading from 'vue-loading-overlay'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '../static/dist/css/adminlte.css'
import '../static/dist/css/mv.css'

Vue.component('v-select', vSelect);
// import Inputmask from 'inputmask';
const VueInputMask = require('vue-inputmask').default

let urlAws = 'https://s3-sa-east-1.amazonaws.com/repmed-fotos/';
Vue.prototype.$urlfoto = urlAws + 'fotos/';
Vue.prototype.$urlfile = urlAws + 'files/';
Vue.prototype.$tbNoData = 'Não há registros disponível';
Vue.prototype.$tbSearchData = 'Não há registros correspondentes à sua solicitação';

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(VueInputMask);
Vue.use(Vuex);
Vue.use(Loading);
Vue.use(VueCharts);

Vue.use(VeeValidate, {
  locale: 'pt_BR',
  dictionary: {
    pt_BR: {
      messages: msg
    }
  },
  fieldsBagName: 'veeFields'
});

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.config.productionTip = false
sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
});
